package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T extends Comparable <T>> extends Iterable<T> {

	Integer getSize();
	
	
	Node<T> getFirst();
	
	Node<T> getTail();
	
	void add(T item);
	
	void addFirst(T item);
	
	
	Node<T> getInPos(int x) throws IndexOutOfBoundsException;
	
	boolean remove(T item);
	
	T search(T id);

	void addInPos(Node<T> insert, int x) throws IndexOutOfBoundsException;


	void add(Node tmp2);



}
