package model.data_structures;



public class Node <T extends Comparable<T>> {
	
	private T item;
	
	private Node<T> last;
	
	private Node<T> next;
	
	
	public Node(T pItem, Node<T> pBefore, Node<T> pAfter) {
		item = pItem;
		last = pBefore;
		next = pAfter;

	}
	
	public Node(T pItem) {
		item = pItem;
	}
	
	public Node(String[] nextLine) {
		// TODO Auto-generated constructor stub
	}

	public T getItem() {
	
	    return item;
		
	}
	
	public Node<T> getLast() {
	
	    return last;
		
	}
	
	public void setLast(Node<T> node) {
		
		last = node;
		
	}
	
	public Node<T> getNext() {
	
	    return next;
		
	}
	
	public void setNext(Node<T> node) {
		
		next = node;
		
	}
	
	
}