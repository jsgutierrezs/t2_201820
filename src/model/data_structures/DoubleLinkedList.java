package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T extends Comparable<T>>  implements DoublyLinkedList {

	//Clase interna Nodo

	
	private int size;
	
	private Node<T> head;
	
	private Node<T> tail;
	
	public DoubleLinkedList()
	{
		size = 0;
		head = null;
		tail = null;
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return size;
	}


	@Override
	public Node<T> getFirst() {
		// TODO Auto-generated method stub
		return head;
	}
	
	@Override
	public Node<T> getTail() {
		
		return tail;
	}

	//agrega al final por default, retorna True si se hace la inserci�n
	@Override
	public void add(Node tmp2) {
		// TODO Auto-generated method stub
		if(head == null) {
			head = new Node<T>((T) tmp2);
			tail = head;
			size++;
		}
		else {
			Node<T> tmp = tail;
			tail = new Node<T>((T) tmp2);
			tmp.setNext(tail);
			tail.setLast(tmp);
			size++;
		}
		
		
	}
	public void addGen (T item) {
		if(head == null) {
			head = new Node<T>((T) item);
			tail = head;
			size++;
		}
		else {
			Node<T> tmp = tail;
			tail = new Node<T>((T) item);
			tmp.setNext(tail);
			tail.setLast(tmp);
			size++;
		}
	}

	//agrega siempre al comienzo
	@Override
	public void addFirst(Comparable item) {
		// TODO Auto-generated method stub
		if(head == null) {
			head = new Node<T>((T) item);
			tail = head;
			size++;

		}
		else {
			Node<T> tmp = head;
			head = new Node<T>((T) item);
			head.setNext(tmp);
			tmp.setLast(head);
			if(tmp.getNext() == null) {
				tmp = tail;
			}
			size++;

		}
	}
	

	@Override
	public boolean remove(Comparable item) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Comparable search(Comparable id) {
		// TODO Auto-generated method stub
		if(head != null) {
			Node<T> tmp = head;
			while (tmp != null) { 
				if(tmp.getItem().equals(id)) {
					return (Comparable) tmp;
				}
				tmp = tmp.getNext();
			}
		}
		return null;
	}

	//retorna el item en la posici�n dada por par�metro, lanza excepci�n si el �ndice es mayor al tama�o de la lista
	@Override
	public Node<T> getInPos(int x) throws IndexOutOfBoundsException {
		// TODO Auto-generated method stub
		if(x > size) throw new IndexOutOfBoundsException("El indice insertado no existe");
		int c = 0;
		if(head != null) {
			Node<T> tmp = head;
			while (tmp != null) {
				if(c == x) return tmp;
				
				tmp = tmp.getNext();
				c++;
			}
		}
		return null;
	}
	@Override
	public void addInPos(Node insert, int x) throws IndexOutOfBoundsException {
		// TODO Auto-generated method stub
		if(x > size) throw new IndexOutOfBoundsException("El indice insertado no existe");
		int c = 0;
		if(head != null) {
			Node<T> tmp = head;
			Node<T> antes = head.getLast();
			Node<T> despues = head.getNext();
			while (tmp != null) {
				if(c == x) {
					insert.setNext(tmp);
					insert.setLast(antes);
					tmp.setLast(insert);
					if(tmp.getNext() == null) tail = tmp;
					size++;
					return;
				}
				antes = tmp;
				tmp = despues;
				despues = despues.getNext();
				c++;
			}
		}
	}
	@Override
	public void add(Comparable item) {
		// TODO Auto-generated method stub
		return;
	}



	

}
