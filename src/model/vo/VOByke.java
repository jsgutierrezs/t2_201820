package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a byke object
 */
public class VOByke implements Comparable<VOByke> {
// id, name, city, latitude, longitude, dpcapacity, online_date.
	
	private int id;
	
	private String name;
	
	private String city;
	
	private String latitude;
	
	private String longitude;
	
	private int dpCapacity;
	
	private String onlineDate;
	
	public VOByke(int pId, String pName, String pCity, String pLatitude, String pLongitude, int pDpCapacity, String pOnlineDate) {
		id = pId;
		name = pName;
		city = pCity;
		latitude = pLatitude;
		longitude = pLongitude;
		dpCapacity = pDpCapacity;
		onlineDate = pOnlineDate;
	}

	public VOByke(String[] nextLine) {
		// TODO Auto-generated constructor stub
		id = Integer.parseInt(nextLine[0]);
		name = nextLine[1];
		city = nextLine[2];
		latitude = nextLine[3];
		longitude = nextLine[4];
		dpCapacity = Integer.parseInt(nextLine[5]);
		onlineDate = nextLine[6];
	}

	/**
	 * @return id_bike - Bike_id
	 */
	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	public String getName() {
		return name;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getLatitude() {
		return latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	
	public int getDpCapacity() {
		return dpCapacity;
	}
	
	public String getOnllineDate() {
		return onlineDate;
	}

	@Override
	public int compareTo(VOByke o) {
		// TODO Auto-generated method stub
		if(this.getId() == o.getId()) return 0;
		if(this.getId() > o.getId()) return 1;
		else return -1;
	}
	
}
