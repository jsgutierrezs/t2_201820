package model.vo;

import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip> {
//  trip_id, start_time, end_time, bikeid, tripduration, from_station_id, from_station_name,
//	to_station_id, to_station_name, usertype, gender, birthyear.
	
	private int tripId;
	
	private String startTime;
	
	private String endTime;
	
	private int bikeId;
	
	private double tripDuration;
	
	private int  startStationId;
	
	private String startStationName;
	
	private int endStationId;
	
	private String endStationName;
	
	private String userType;
	
	private String gender;
	
	private short birthYear;
	
	
	public VOTrip(int pTripId, String pStartTime, String pEndTime, int pBikeId, double pTripDuration,
			int pStartStationId, String pStartStationName, int pEndStationId, String pEndStationName,
			String pUserType, String pGender, short pBirthYear) {
		tripId = pTripId;
		startTime = pStartTime;
		endTime = pEndTime;
		bikeId = pBikeId;
		tripDuration = pTripDuration;
		startStationId = pStartStationId;
		startStationName = pStartStationName;
		endStationId = pEndStationId;
		endStationName = pEndStationName;
		userType = pUserType;
		gender = pGender;
		birthYear = pBirthYear;
		
	}
	
	
	public VOTrip(String[] nextLine) {
		// TODO Auto-generated constructor stub
		tripId = Integer.parseInt(nextLine[0]);
		startTime = nextLine[1];
		endTime = nextLine[2];
		bikeId = Integer.parseInt(nextLine[3]);
		tripDuration = Double.parseDouble(nextLine[4]);
		startStationId = Integer.parseInt(nextLine[5]);
		startStationName = nextLine[6];
		endStationId = Integer.parseInt(nextLine[7]);
		endStationName = nextLine[7];
		userType = nextLine[8];
		gender = nextLine[9];
		birthYear = Short.parseShort(nextLine[10]);
		
	}


	/**
	 * @return id - Trip_id
	 */
	public int getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	public String getStartTime() {
		return startTime;
	}
	
	public String getEndTime() {
		return endTime;
	}
	
	public int getBikeId() {
		return bikeId;
	}

	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripDuration;
	}

	
	public int getFromStationId() {
		return startStationId;
	}
	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return startStationName;
	}
	
	public int getToStationId() {
		return endStationId;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return endStationName;
	}
	
	public String getUserType() {
		return userType;
	}
	
	public String getGender() {
		return gender;
	}
	
	public short getBirthYear() {
		return birthYear;
	}

	@Override
	public int compareTo(VOTrip arg0) {
		// TODO Auto-generated method stub
		if(this.getTripId() == arg0.getTripId()) return 0;
		if(this.getTripId() > arg0.getTripId()) return 1;
		else return -1;
	}
}
