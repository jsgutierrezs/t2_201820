package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;

public class DivvyTripsManager implements IDivvyTripsManager {

	
	public final static String Q4 = "./data/Divvy_Trips_2017_Q4.csv";
	
	public final static String Q3Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";
	
	private DoubleLinkedList<VOTrip> myEntriesQ4 = new DoubleLinkedList<>();
	
	private DoubleLinkedList<VOByke> myEntriesQ3Q4 = new DoubleLinkedList<>();
	
	
	//Debo crear una clase para guardar la informacion que se va a cargar y
	//as� poderla guardar facilmente en la posicion de la lista
	
	DoubleLinkedList<VOTrip> lista = new DoubleLinkedList<VOTrip>();
	
	public void loadStations (String stationsFile) throws IOException {
		// TODO Auto-generated method stub
		CSVReader reader = new CSVReader( new FileReader(Q3Q4));
		String[] nextLine;
		int i = 1;
		while((nextLine = reader.readNext()) != null) {
			myEntriesQ3Q4.add(new Node(new VOByke(nextLine)));
			System.out.println(i);
			i++;
		}
		
	}

	
	public void loadTrips (String tripsFile) throws IOException {
		// TODO Auto-generated method stub
	     CSVReader reader = new CSVReader(new FileReader(Q4));
	     String[] nextLine;
	     int i = 1;
	     while((nextLine = reader.readNext()) != null ) {
	    	myEntriesQ4.add(new Node (new VOTrip(nextLine)));
	    	System.out.println(i);
	    	i++;
	    	 
	    	// myEntries.add(new VOTrip(nextLine));

	     }
	     //DoubleLinkedList<String> myEntries = (DoubleLinkedList<String>) reader.readAll();
	   
	}
	
	@Override
	public DoubleLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VOTrip> res = null;
		Node tmp = myEntriesQ4.getFirst();
		while(tmp != null) {
			if(((VOTrip) tmp.getItem()).getGender().equals(gender)) res.add(tmp);
			tmp = tmp.getNext();
		}
		
		return res;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VOTrip> res = null;
		Node tmp = myEntriesQ4.getFirst();
		while(tmp != null) {
			if(((VOTrip) tmp.getItem()).getToStationId() == stationID) res.add(tmp);
			tmp = tmp.getNext();
		}
		
		return res;
	}	


}
